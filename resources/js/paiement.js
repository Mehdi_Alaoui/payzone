var annee =["2016","2017","2018","2019","2020","2021","2022","2023","2024","2025"];
var mois =["01","02","03","04","05","06","07","08","09","10","11","12"];

$('#CrediCard_black').hide();
$('#CrediCard_skyblue').hide();
$("#CrediCard_cvv").hide();
$("#CrediCard_legal").hide();

$('#InputErrorCrediCardExpiration').hide();
$('#InputErrorCrediCardNumber').hide();
$("#InputErrorCrediCardCode").hide();
$("#InputErrorCrediCardName").hide();
$("#InputErrorCrediCardExpiratAnnee").hide();
$("#InputErrorCrediCardExpiratMois").hide();


$('#CrediCard_blackResp').hide();
$('#CrediCard_skyblueResp').hide();
$('#CrediCard_BtnRetoureResp').hide();
$('#CrediCard_BtnValiderResp').hide();
$('#CrediCard_BtnRetour').hide();
$('#CrediCard_BtnValider').hide();
$("#CrediCard_cvvResp").hide();
$("#CrediCard_legalResp").hide();


$('#InputErrorCrediCardExpirationResp').hide();
$('#InputErrorCrediCardNumberResp').hide();
$("#InputErrorCrediCardCodeResp").hide();
$("#InputErrorCrediCardNameResp").hide();
$("#InputErrorCrediCardExpiratAnneeResp").hide();
$("#InputErrorCrediCardExpiratMoisResp").hide();


$("#cardNumNAvResp1").css({ color: "#111111" });
$('#cardNumNavDivResp2').hide();
$('#cardNumNavDivResp3').hide();
$('#cardNumNavDivResp4').hide();

var CardPosition = true;
var CardPositionResp = true;

$( document ).ready(function() {
    $(" #creditCardExpiration,#creditCardCodeXXX,#card-number,#card-number-1,#card-number-2,#card-number-3," +
        "#card-expiration-month,#card-expiration-year,#NamecreditCard,#creditCardNumberCode,#CreditCardSuiv,#CrediCard_BtnRetour").click(function () {
        if (this.id == "creditCardExpiration" || this.id == "card-number" || this.id == "card-number-1"||
            this.id == "card-number-2" || this.id == "card-number-3" || this.id == "NamecreditCard" ||
            this.id == "card-expiration-month" || this.id == "card-expiration-year"|| this.id == "creditCardNumberCode"|| this.id == "CrediCard_BtnRetour") {
            if(!CardPosition){
                $(".CreditCardCadre").addClass("flip-scale-2-hor-top");
                $(".CrediCard_cvv").addClass("flip-scale-2-hor-top");
                $(".CrediCard_skyblue").addClass("flip-scale-2-hor-top");
                $(".CrediCard_black").addClass("flip-scale-2-hor-top");
                $(".CrediCard_BtnRetour").addClass("flip-scale-2-hor-top");
                $(".CrediCard_BtnValider").addClass("flip-scale-2-hor-top");
                // $(".CrediCard_legal").addClass("flip-scale-2-hor-top");
                $("#CrediCard_legal").hide();
                setTimeout(function () {
                    $('#CreditCardContent').hide();
                    $('#CreditCardFooter').hide();
                    $("#CrediCard_cvv").hide();
                    $("#CrediCard_skyblue").hide();
                    $("#CrediCard_black").hide();
                    $("#CrediCard_BtnRetour").hide();
                    $("#CrediCard_BtnValider").hide();
                    //   $("#CrediCard_legal").hide();
                    $(".CreditCardCadre").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_cvv").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_skyblue").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_black").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_legal").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_BtnRetour").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_BtnValider").removeClass("flip-scale-2-hor-top");
                    $("#CreditCardContent").show();
                    $("#CreditCardFooter").show();
                }, 500);
                CardPosition = true;
            }
        }
        else if (this.id == "creditCardCodeXXX" ||  this.id == "CreditCardSuiv" ) {
            if(CardPosition){
                $(".CreditCardCadre").addClass("flip-scale-2-hor-bottom");
                $(".CreditCardContent").addClass("flip-scale-2-hor-bottom");
                $(".CreditCardFooter").addClass("flip-scale-2-hor-bottom");
                setTimeout(function () {
                    $('#CreditCardContent').hide();
                    $('#CreditCardFooter').hide();
                    $(".CreditCardCadre").removeClass("flip-scale-2-hor-bottom");
                    $(".CreditCardContent").removeClass("flip-scale-2-hor-bottom");
                    $(".CreditCardFooter").removeClass("flip-scale-2-hor-bottom");
                    $("#CrediCard_black").show();
                    $("#CrediCard_skyblue").show();
                    $("#CrediCard_cvv").show();
                    $("#CrediCard_legal").show();
                    $("#CrediCard_BtnRetour").show();
                    $("#CrediCard_BtnValider").show();
                }, 500);
                CardPosition=false;
            }
        }
    });

    $(" #creditCardExpirationResp,#creditCardCodeXXXResp,#card-numberResp,#card-number-1Resp,#card-number-2Resp,#card-number-3Resp," +
        "#card-expiration-monthResp,#card-expiration-yearResp,#NamecreditCardResp,#creditCardNumberCodeResp," +
        "#cardNumNAvResp1,#cardNumNAvResp2,#cardNumNAvResp3,#CreditCardSuivRsp,#CrediCard_BtnRetoureResp").click(function () {
        if (this.id == "creditCardExpirationResp" || this.id == "card-numberResp" || this.id == "card-number-1Resp"||
            this.id == "card-number-2Resp" || this.id == "card-number-3Resp" || this.id == "NamecreditCardResp" ||
            this.id == "card-expiration-monthResp" || this.id == "card-expiration-yearResp"|| this.id == "creditCardNumberCodeResp" ||
            this.id == "cardNumNAvResp1"||this.id == "cardNumNAvResp2"|| this.id == "cardNumNAvResp2" || this.id == "cardNumNAvResp3" || this.id == "CrediCard_BtnRetoureResp") {
            if(!CardPositionResp){
                $(".CreditCardCadreResp").addClass("flip-scale-2-hor-top");
                $(".CrediCard_cvvResp").addClass("flip-scale-2-hor-top");
                $(".CrediCard_skyblueResp").addClass("flip-scale-2-hor-top");
                $(".CrediCard_BtnRetoureResp").addClass("flip-scale-2-hor-top");
                $(".CrediCard_BtnValiderResp").addClass("flip-scale-2-hor-top");
                $(".CrediCard_blackResp").addClass("flip-scale-2-hor-top");
                // $(".CrediCard_legal").addClass("flip-scale-2-hor-top");
                $("#CrediCard_legalResp").hide();
                setTimeout(function () {
                    $('#CreditCardContentResp').hide();
                    $('#CreditCardFooterResp').hide();
                    $("#CrediCard_cvvResp").hide();
                    $("#CrediCard_skyblueResp").hide();
                    $("#CrediCard_BtnRetoureResp").hide();
                    $("#CrediCard_BtnValiderResp").hide();
                    $("#CrediCard_blackResp").hide();
                    //   $("#CrediCard_legal").hide();
                    $(".CreditCardCadreResp").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_cvvResp").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_skyblueResp").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_BtnRetoureResp").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_BtnValiderResp").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_blackResp").removeClass("flip-scale-2-hor-top");
                    $(".CrediCard_legalResp").removeClass("flip-scale-2-hor-top");
                    $("#CreditCardContentResp").show();
                    $("#CreditCardFooterResp").show();
                }, 500);
                CardPositionResp = true;
            }
        }
        else if (this.id == "creditCardCodeXXXResp"||  this.id == "CreditCardSuivRsp" ) {
            if(CardPositionResp){
                $(".CreditCardCadreResp").addClass("flip-scale-2-hor-bottom");
                $(".CreditCardContentResp").addClass("flip-scale-2-hor-bottom");
                $(".CreditCardFooterResp").addClass("flip-scale-2-hor-bottom");
                setTimeout(function () {
                    $('#CreditCardContentResp').hide();
                    $('#CreditCardFooterResp').hide();
                    $(".CreditCardCadreResp").removeClass("flip-scale-2-hor-bottom");
                    $(".CreditCardContentResp").removeClass("flip-scale-2-hor-bottom");
                    $(".CreditCardFooterResp").removeClass("flip-scale-2-hor-bottom");
                    $("#CrediCard_blackResp").show();
                    $("#CrediCard_skyblueResp").show();
                    $("#CrediCard_BtnRetoureResp").show();
                    $("#CrediCard_BtnValiderResp").show();
                    $("#CrediCard_cvvResp").show();
                    $("#CrediCard_legalResp").show();
                }, 500);
                CardPositionResp=false;
            }
        }
    });

});

function  onChangeCreditCardNumCode(){
    const reg = /^\d+$/;
    var cardCode = $("#creditCardNumberCode").val().replace(/\ /g, '');

    $("#creditCardNumberCode").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));
    $("#creditCardNumber").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));

    $("#creditCardNumberCodeResp").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));
    $("#creditCardNumberResp").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));

    if( $("#creditCardNumberCode").val().replace(/\ /g, '').length == 16 && reg.test($("#creditCardNumberCode").val().replace(/\ /g, ''))){
        $('#InputErrorCrediCardNumber').hide();
        $("#ImgArrowStep1").attr("src","./resources/images/suivant-btn-valide.png");
        $("#creditCardNumberResp").css('background-color','#fff');
        $("#creditCardNumber").css('background-color','#fff');
    }else{
        $('#InputErrorCrediCardNumber').show();
        $("#ImgArrowStep1").attr("src","./resources/images/suivant-btn-nnvalide.png");
        $("#CreditCardNameResp").css('background-color','#b6b6b6');
        $("#creditCardNumber").css('background-color','#b6b6b6');
    }
}

function  onChangeCreditCardNumCodes(){
    const reg = /^\d+$/;
    var cardCode = $("#creditCardNumber").val().replace(/\ /g, '');

    $("#creditCardNumberCode").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));
    $("#creditCardNumber").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));

    $("#creditCardNumberCodeResp").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));
    $("#creditCardNumberResp").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));

    if( $("#creditCardNumber").val().replace(/\ /g, '').length == 16 && reg.test($("#creditCardNumber").val().replace(/\ /g, '')) ){
        $('#InputErrorCrediCardNumber').hide();
        $("#ImgArrowStep1").attr("src","./resources/images/suivant-btn-valide.png");
        $("#CreditCardNameResp").css('background-color','#fff');
        $("#creditCardNumber").css('background-color','#fff');
    }else{
        $('#InputErrorCrediCardNumber').show();
        $("#ImgArrowStep1").attr("src","./resources/images/suivant-btn-nnvalide.png");
        $("#CreditCardNameResp").css('background-color','#b6b6b6');
        $("#creditCardNumber").css('background-color','#b6b6b6');
    }
}


function onChangecreditCardMois(){
    $("#creditCardMM").val($("#card-expiration-month").val());
    $("#creditCardMMResp").val($("#card-expiration-month").val());
    onChangecreditCardMoiResp();
    onChangecreditCardMoi();
}

function onChangecreditCardAnnes(){
    $("#creditCardYYYY").val($("#card-expiration-year").val());
    $("#creditCardYYYYResp").val($("#card-expiration-year").val());
    onChangecreditCardAnneResp();
    onChangecreditCardAnne();
}

function onChangecreditCardMoi(){
    if(mois.includes($("#creditCardMM").val())){
        $("#card-expiration-month").val($("#creditCardMM").val());
        $('#InputErrorCrediCardExpiratMois').hide();
        onChangecreditCardMois();
    }else{
        $('#InputErrorCrediCardExpiratMois').show();
        $('#card-expiration-year').val([]);
    }
}

function onChangecreditCardAnne(){
    if(annee.includes($("#creditCardYYYY").val())){
        $("#card-expiration-year").val($("#creditCardYYYY").val());
        $('#InputErrorCrediCardExpiratAnnee').hide();
        onChangecreditCardAnnes();
    }else{
        $('#card-expiration-month').val([]);
        $('#InputErrorCrediCardExpiratAnnee').show();
    }
}

function onChangecreditCardCvv(){
    const reg = /^\d+$/;
    if( $('#creditCardCodeXXX').val().length == 3 && reg.test($('#creditCardCodeXXX').val())){
        $('#InputErrorCrediCardCode').hide();
        $("#ImgArrowStep4").attr("src","./resources/images/valider-enable.png");
        $("#CrediCard_BtnValiderResp").attr("src","./resources/images/valider-enable.png");
        $("#CrediCard_BtnValider").attr("src","./resources/images/valider-enable.png");
        $("#creditCardCvv").css('background-color','#fff');
        $("#creditCardCvvResp").css('background-color','#fff');
    }else{
        $('#InputErrorCrediCardCode').show();
        $("#ImgArrowStep4").attr("src","./resources/images/valider-disable.png");
        $("#CrediCard_BtnValiderResp").attr("src","./resources/images/valider-disable.png");
        $("#CrediCard_BtnValider").attr("src","./resources/images/valider-disable.png");
        $("#creditCardCvv").css('background-color','#b6b6b6');
        $("#creditCardCvvResp").css('background-color','#b6b6b6');
    }
    $("#creditCardCvv").val($("#creditCardCodeXXX").val());
    $("#creditCardCvvResp").val($("#creditCardCodeXXX").val());
    $("#creditCardCodeXXXResp").val($("#creditCardCodeXXX").val());
}

function onChangecreditCardCvv2(){
    const reg = /^\d+$/;
    if( $('#creditCardCvv').val().length == 3 && reg.test($('#creditCardCvv').val())){
        $("#ImgArrowStep4").attr("src","./resources/images/valider-enable.png");
        $("#CrediCard_BtnValiderResp").attr("src","./resources/images/valider-enable.png");
        $('#InputErrorCrediCardCode').hide();
        $("#creditCardCvv").css('background-color','#fff');
        $("#creditCardCvvResp").css('background-color','#fff');
    }else{
        $('#InputErrorCrediCardCode').show();
        $("#ImgArrowStep4").attr("src","./resources/images/valider-disable.png");
        $("#CrediCard_BtnValiderResp").attr("src","./resources/images/valider-disable.png");
        $("#creditCardCvv").css('background-color','#b6b6b6');
        $("#creditCardCvvResp").css('background-color','#b6b6b6');
    }
    $("#creditCardCodeXXX").val($("#creditCardCvv").val());
    $("#creditCardCvvResp").val($("#creditCardCvv").val());
    $("#creditCardCodeXXXResp").val($("#creditCardCvv").val());
}

function onChangecreditCardName(){
    if( $('#NamecreditCard').val().length > 3 ){
        $('#InputErrorCrediCardName').hide();
        $("#CreditCardName").css('background-color','#fff');
        $("#CreditCardNameResp").css('background-color','#fff');
    }else{
        $('#InputErrorCrediCardName').show();
        $("#CreditCardName").css('background-color','#b6b6b6');
        $("#CreditCardNameResp").css('background-color','#b6b6b6');
    }
    $("#CreditCardName").text($("#NamecreditCard").val());
    $("#CreditCardNameResp").text($("#NamecreditCard").val());
    $("#CreditCardName").val($("#NamecreditCard").val());
    $("#CreditCardNameResp").val($("#NamecreditCard").val());
    $("#NamecreditCardResp").val($("#NamecreditCard").val());
}

function  cardNumNAvResp(x){
    if(x==1){
        $("#cardNumNAvResp1").css({ color: "#111111" });
        $("#cardNumNAvResp2").css({ color: "#b6b6b6" });
        $("#cardNumNAvResp3").css({ color: "#b6b6b6" });
        $("#cardNumNAvResp4").css({ color: "#b6b6b6" });
        $('#cardNumNavDivResp1').show();
        $('#cardNumNavDivResp2').hide();
        $('#cardNumNavDivResp3').hide();
        $('#cardNumNavDivResp4').hide();
        $( "nav.CardNav" ).scrollLeft( $("#cardNumNAvResp1").offset().left );
    }else if(x==2){
        if(CreditCardNumCodeRespValidate()){
            $("#cardNumNAvResp1").css({ color: "#1d72db" });
            $("#cardNumNAvResp2").css({ color: "#111111" });
            $("#cardNumNAvResp3").css({ color: "#b6b6b6" });
            $("#cardNumNAvResp4").css({ color: "#b6b6b6" });
            $('#cardNumNavDivResp1').hide();
            $('#cardNumNavDivResp2').show();
            $('#cardNumNavDivResp3').hide();
            $('#cardNumNavDivResp4').hide();
            $( "nav.CardNav" ).scrollLeft( $("#cardNumNAvResp2").offset().left -15);
        }
    }else if(x==3){
        if( annee.includes($("#creditCardYYYYResp").val()) && mois.includes($("#creditCardMMResp").val())) {
            $("#cardNumNAvResp1").css({ color: "#1d72db" });
            $("#cardNumNAvResp2").css({ color: "#1d72db" });
            $("#cardNumNAvResp3").css({ color: "#111111" });
            $("#cardNumNAvResp4").css({ color: "#b6b6b6" });
            $('#cardNumNavDivResp1').hide();
            $('#cardNumNavDivResp2').hide();
            $('#cardNumNavDivResp3').show();
            $('#cardNumNavDivResp4').hide();
            $("#ImgArrowStep2").attr("src","./resources/images/suivant-btn-valide.png");
            $( "nav.CardNav" ).scrollLeft( $("#cardNumNAvResp3").offset().left );
        }

    }else if(x==4){
        if( $('#NamecreditCardResp').val().length > 3 ) {
            $("#cardNumNAvResp1").css({color: "#1d72db"});
            $("#cardNumNAvResp2").css({color: "#1d72db"});
            $("#cardNumNAvResp3").css({color: "#1d72db"});
            $("#cardNumNAvResp4").css({color: "#111111"});
            $('#cardNumNavDivResp1').hide();
            $('#cardNumNavDivResp2').hide();
            $('#cardNumNavDivResp3').hide();
            $('#cardNumNavDivResp4').show();
            $( "nav.CardNav" ).scrollLeft( $("#cardNumNAvResp4").offset().left );
        }
    }else if(x==5){
        if( $('#creditCardCodeXXXResp').val().length == 3 ) {
            clickSubmitCreditCardForm();
        }
    }
}


function  CreditCardNumCodeRespValidate(){
    const reg = /^\d+$/;
    var cardCode = $("#creditCardNumberCodeResp").val().replace(/\ /g, '');

    $("#creditCardNumberCodeResp").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));
    $("#creditCardNumberResp").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));
    $("#creditCardNumberCode").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));
    $("#creditCardNumber").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));

    if( $("#creditCardNumberCodeResp").val().replace(/\ /g, '').length == 16 && reg.test($("#creditCardNumberCodeResp").val().replace(/\ /g, ''))){
        return true;
    }else{
        return false;
    }
}


function  onChangeCreditCardNumCodeResp(){

    if( CreditCardNumCodeRespValidate()){
        $('#InputErrorCrediCardNumberResp').hide();
        $("#ImgArrowStep1").attr("src","./resources/images/suivant-btn-valide.png");
        $("#creditCardNumberResp").css('background-color','#fff');
    }else{
        $('#InputErrorCrediCardNumberResp').show();
        $("#ImgArrowStep1").attr("src","./resources/images/suivant-btn-nnvalide.png");
        $("#creditCardNumberResp").css('background-color','#b6b6b6');
    }
}

function  onChangeCreditCardNumCodesResp(){
    const reg = /^\d+$/;
    var cardCode = $("#creditCardNumberResp").val().replace(/\ /g, '');

    $("#creditCardNumberCodeResp").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));
    $("#creditCardNumberResp").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));

    $("#creditCardNumberCode").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));
    $("#creditCardNumber").val(cardCode.toString().replace(/\B(?=(\d{4})+(?!\d))/g, " "));


    if( $("#creditCardNumberResp").val().replace(/\ /g, '').length == 16 && reg.test($("#creditCardNumberResp").val().replace(/\ /g, '')) ){
        $('#InputErrorCrediCardNumberResp').hide();
        $("#ImgArrowStep1").attr("src","./resources/images/suivant-btn-valide.png");
        $("#creditCardNumberResp").css('background-color','#fff');
        $("#creditCardNumber").css('background-color','#fff');
        $("#creditCardNumberResp").css('border','1px solid #007bff');
    }else{
        $('#InputErrorCrediCardNumberResp').show();
        $("#ImgArrowStep1").attr("src","./resources/images/suivant-btn-nnvalide.png");
        $("#creditCardNumberResp").css('background-color','#b6b6b6');
        $("#creditCardNumber").css('background-color','#b6b6b6');
        $("#creditCardNumberResp").css('border','1px solid red');
    }
}


function onChangecreditCardMoisResp(){
    $("#creditCardMM").val($("#card-expiration-monthResp").val());
    $("#creditCardMMResp").val($("#card-expiration-monthResp").val());
    onChangecreditCardMoiResp();
    onChangecreditCardMoi();

}

function onChangecreditCardAnnesResp(){
    $("#creditCardYYYYResp").val($("#card-expiration-yearResp").val());
    $("#creditCardYYYY").val($("#card-expiration-yearResp").val());
    onChangecreditCardAnneResp();
    onChangecreditCardAnne();

}

function onChangecreditCardMoiResp(){
    if(mois.includes($("#creditCardMMResp").val())){
        $("#card-expiration-monthResp").val($("#creditCardMMResp").val());
        $("#card-expiration-month").val($("#creditCardMMResp").val());
        $("#creditCardMM").val($("#creditCardMMResp").val());
        $('#InputErrorCrediCardExpiratMoisResp').hide();
        $("#creditCardMM").css('background-color','#fff');
        $("#creditCardMMResp").css('background-color','#fff');
        $("#creditCardMMResp").css('border','1px solid #007bff');
    }else{
        $('#InputErrorCrediCardExpiratMoisResp').show();
        $('#card-expiration-monthResp').val([]);
        $('#card-expiration-month').val([]);
        $("#ImgArrowStep2").attr("src","./resources/images/suivant-btn-nnvalide.png");
        $("#creditCardMM").css('background-color','#b6b6b6');
        $("#creditCardMMResp").css('background-color','#b6b6b6');
        $("#creditCardMMResp").css('border','1px solid red');
    }
    if( annee.includes($("#creditCardYYYYResp").val()) && mois.includes($("#creditCardMMResp").val())) {
        $("#ImgArrowStep2").attr("src","./resources/images/suivant-btn-valide.png");
    }
}

function onChangecreditCardAnneResp(){
    if(annee.includes($("#creditCardYYYYResp").val())){
        $("#card-expiration-yearResp").val($("#creditCardYYYYResp").val());
        $("#card-expiration-year").val($("#creditCardYYYYResp").val());
        $("#creditCardYYYY").val($("#creditCardYYYYResp").val());
        $('#InputErrorCrediCardExpiratAnneeResp').hide();
        $("#creditCardYYYYResp").css('background-color','#fff');
        $("#creditCardYYYY").css('background-color','#fff');
        $("#creditCardYYYYResp").css('border','1px solid #007bff');
    }else{
        $('#card-expiration-yearResp').val([]);
        $('#InputErrorCrediCardExpiratAnneeResp').show();
        $("#creditCardYYYYResp").css('background-color','#b6b6b6');
        $("#creditCardYYYY").css('background-color','#b6b6b6');
        $("#creditCardYYYYResp").css('border','1px solid red');
    }
    if( annee.includes($("#creditCardYYYYResp").val()) && mois.includes($("#creditCardMMResp").val())) {
        $("#ImgArrowStep2").attr("src","./resources/images/suivant-btn-valide.png");
    }
}

function onChangecreditCardCvvResp(){
    const reg = /^\d+$/;
    if( $('#creditCardCodeXXXResp').val().length == 3 && reg.test($('#creditCardCodeXXXResp').val())){
        $('#InputErrorCrediCardCodeResp').hide();
        $("#ImgArrowStep4").attr("src","./resources/images/valider-enable.png");
        $("#CrediCard_BtnValiderResp").attr("src","./resources/images/valider-enable.png");
        $("#creditCardCvv").css('background-color','#fff');
        $("#creditCardCvvResp").css('background-color','#fff');
    }else{
        $('#InputErrorCrediCardCodeResp').show();
        $("#ImgArrowStep4").attr("src","./resources/images/valider-disable.png");
        $("#CrediCard_BtnValiderResp").attr("src","./resources/images/valider-disable.png");
        $("#creditCardCvv").css('background-color','#b6b6b6');
        $("#creditCardCvvResp").css('background-color','#b6b6b6');
    }
    $("#creditCardCodeXXX").val($("#creditCardCodeXXXResp").val());
    $("#creditCardCvvResp").val($("#creditCardCodeXXXResp").val());
    $("#creditCardCodeXXXResp").val($("#creditCardCodeXXXResp").val());
    $("#creditCardCvv").val($("#creditCardCodeXXXResp").val());
}

function onChangecreditCardCvv2Resp(){
    const reg = /^\d+$/;
    if( $('#creditCardCvvResp').val().length == 3 && reg.test($('#creditCardCvvResp').val())){
        $('#InputErrorCrediCardCodeResp').hide();
        $("#ImgArrowStep4").attr("src","./resources/images/valider-enable.png");
        $("#CrediCard_BtnValiderResp").attr("src","./resources/images/valider-enable.png");
        $("#creditCardCvv").css('background-color','#fff');
        $("#creditCardCvvResp").css('background-color','#fff');
    }else{
        $('#InputErrorCrediCardCodeResp').show();
        $("#ImgArrowStep4").attr("src","./resources/images/valider-disable.png");
        $("#CrediCard_BtnValiderResp").attr("src","./resources/images/valider-disable.png");
        $("#creditCardCvv").css('background-color','#b6b6b6');
        $("#creditCardCvvResp").css('background-color','#b6b6b6');
    }
    $("#creditCardCodeXXX").val($("#creditCardCvvResp").val());
    $("#creditCardCvvResp").val($("#creditCardCvvResp").val());
    $("#creditCardCodeXXXResp").val($("#creditCardCvvResp").val());
    $("#creditCardCvv").val($("#creditCardCvvResp").val());
}

function onChangecreditCardNameResp(){
    if( $('#NamecreditCardResp').val().length > 3 ){
        $('#InputErrorCrediCardNameResp').hide();
        $("#ImgArrowStep3").attr("src","./resources/images/suivant-btn-valide.png");
        $("#creditCardMMResp").css('background-color','#fff');
        $("#CreditCardNameResp").css('background-color','#fff');
    }else{
        $('#InputErrorCrediCardNameResp').show();
        $("#ImgArrowStep3").attr("src","./resources/images/suivant-btn-nnvalide.png");
        $("#creditCardMMResp").css('background-color','#b6b6b6');
        $("#CreditCardNameResp").css('background-color','#b6b6b6');
    }
    $("#CreditCardName").text($("#NamecreditCardResp").val());
    $("#CreditCardNameResp").text($("#NamecreditCardResp").val());
    $("#CreditCardName").val($("#NamecreditCardResp").val());
    $("#CreditCardNameResp").val($("#NamecreditCardResp").val());
    $("#NamecreditCard").val($("#NamecreditCardResp").val());

}


function onChangecreditCardName2(){
    if( $('#CreditCardName').val().length > 3 ){
        $('#InputErrorCrediCardName').hide();
        $("#CreditCardName").css('background-color','#fff');
        $("#CreditCardNameResp").css('background-color','#fff');
    }else{
        $('#InputErrorCrediCardName').show();
        $("#CreditCardName").css('background-color','#b6b6b6');
        $("#CreditCardNameResp").css('background-color','#b6b6b6');
    }
    $("#NamecreditCard").text($("#CreditCardName").val());
    $("#CreditCardNameResp").text($("#CreditCardName").val());
    $("#CreditCardName").val($("#CreditCardName").val());
    $("#CreditCardNameResp").val($("#CreditCardName").val());
    $("#NamecreditCardResp").val($("#CreditCardName").val());
    $("#NamecreditCard").val($("#CreditCardName").val());

}


function onChangecreditCardNameResp2(){
    if( $('#CreditCardNameResp').val().length > 3 ){
        $('#InputErrorCrediCardName').hide();
        $("#ImgArrowStep3").attr("src","./resources/images/suivant-btn-valide.png");
        $("#CreditCardNameResp").css('background-color','#fff');
        $("#CreditCardNameResp").css('border','1px solid #007bff');
    }else{
        $('#InputErrorCrediCardName').show();
        $("#ImgArrowStep3").attr("src","./resources/images/suivant-btn-nnvalide.png");
        $("#CreditCardNameResp").css('background-color','#b6b6b6');
        $("#CreditCardNameResp").css('border','1px solid red');
    }
    $("#NamecreditCard").text($("#CreditCardNameResp").val());
    $("#CreditCardName").text($("#CreditCardNameResp").val());
    $("#CreditCardName").val($("#CreditCardNameResp").val());
    $("#CreditCardName").val($("#CreditCardNameResp").val());
    $("#NamecreditCardResp").val($("#CreditCardNameResp").val());
    $("#NamecreditCard").val($("#CreditCardNameResp").val());

}



$('#creditCardNumberCodeResp,#creditCardNumberResp,#creditCardMMResp,#creditCardYYYYResp').on('focus', function() {
    $("html, body").animate({ scrollTop: $("#creditCardNumberCodeResp").offset().top - 290}, 300);
});


function  clickSubmitCreditCardForm(){
    console.log("UserinfoPrenom :",$("#UserinfoPrenom").val());
    console.log("UserinfoEmail :",$("#UserinfoEmail").val());
    console.log("UserinfoTEl :",$("#UserinfoTEl").val());
    console.log("Numero de carte :",$("#creditCardNumberCode").val().replace(/\ /g, ''));
    console.log("Mois :",$("#card-expiration-month").val());
    console.log("Annee :",$("#card-expiration-year").val());
    console.log("Carte Nom prenom :",$("#NamecreditCard").val());
    console.log("CCV :",$("#creditCardCodeXXX").val());
}
