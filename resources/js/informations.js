$('#InputErrorPrenom').hide();
$('#InputErrorNom').hide();
$("#InputErrorEmail").hide();
$("#InputErrorPhone").hide();

function  onChangePrenom(){
    var prenom = document.getElementById('UserinfoPrenom').value;
    if (prenom.length > 2) {
        document.getElementById('InputErrorPrenom').style.display= "none";
        document.getElementById('UserinfoPrenom').style.border= "1px solid #007bff";
        return true;
    }else{
        document.getElementById('InputErrorPrenom').style.display= "block";
        document.getElementById('UserinfoPrenom').style.border= "1px solid red";
        return false;
    }
}



function  onChangeEmail(){
    var email = document.getElementById('UserinfoEmail').value;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    re.test(String(email).toLowerCase());
    if(re.test(String(email).toLowerCase())){
        document.getElementById('InputErrorEmail').style.display= "none";
        document.getElementById('UserinfoEmail').style.border= "1px solid #007bff";
        return true;
    }else{
        document.getElementById('InputErrorEmail').style.display= "block";
        document.getElementById('UserinfoEmail').style.border= "1px solid red";
        return false;
    }
}

function  onChangePhone(){
    var phone = document.getElementById('UserinfoTEl').value;
    const re = /^0(6|5|7)(\d{8})$/;
    if(phone.length !== 10 && !re.test(phone)){
        document.getElementById('InputErrorPhone').style.display= "block";
        document.getElementById('UserinfoTEl').style.border= "1px solid red";
        return false;
    }else{
        document.getElementById('InputErrorPhone').style.display= "none";
        document.getElementById('UserinfoTEl').style.border= "1px solid #007bff";
        return true;
    }
}

function clickSubmitForm(){
    onChangePrenom();
    onChangeEmail();
    onChangePhone();
    if(onChangePrenom()  && onChangeEmail() && onChangePhone()){
        $('#profile-tab').tab('show');
        $("#CreditCardName").val($("#UserinfoPrenom").val());
        $("#NamecreditCard").val($("#UserinfoPrenom").val());
        $("#NamecreditCardResp").val($("#UserinfoPrenom").val());
        $("#CreditCardName").text($("#NamecreditCard").val());
        $("#CreditCardNameResp").text($("#NamecreditCard").val());
    }

}